<?php
/**
 * @file
 * sector_members_directory.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function sector_members_directory_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['facetapi-BRD81QiwkLYy1wPIqUOCapOOW6m64B0W'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'BRD81QiwkLYy1wPIqUOCapOOW6m64B0W',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sector_starter' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sector_starter',
        'weight' => 0,
      ),
      'shiny' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'shiny',
        'weight' => 0,
      ),
    ),
    'title' => 'Directory member type',
    'visibility' => 1,
  );

  $export['facetapi-MiTxGBk1Nn5HLQT1zHoGqLOiWRKdA14f'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'MiTxGBk1Nn5HLQT1zHoGqLOiWRKdA14f',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sector_starter' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sector_starter',
        'weight' => 0,
      ),
      'shiny' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'shiny',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
