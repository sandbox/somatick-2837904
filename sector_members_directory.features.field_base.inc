<?php
/**
 * @file
 * sector_members_directory.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function sector_members_directory_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_directory_contacts'.
  $field_bases['field_directory_contacts'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_directory_contacts',
    'global_block_settings' => 1,
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'entityqueue' => array(
            'status' => 0,
          ),
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(
          'contact' => 'contact',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_directory_locations'.
  $field_bases['field_directory_locations'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_directory_locations',
    'global_block_settings' => 1,
    'indexes' => array(
      'lid' => array(
        0 => 'lid',
      ),
    ),
    'locked' => 0,
    'module' => 'location_cck',
    'settings' => array(
      'gmap_macro' => '[gmap |zoom=13]',
      'gmap_marker' => 'big red',
      'location_settings' => array(
        'display' => array(
          'hide' => array(
            'additional' => 'additional',
            'city' => 0,
            'coords' => 'coords',
            'country' => 0,
            'country_name' => 0,
            'email' => 0,
            'fax' => 0,
            'locpick' => 0,
            'map_link' => 'map_link',
            'name' => 0,
            'phone' => 0,
            'postal_code' => 0,
            'province' => 'province',
            'province_name' => 0,
            'street' => 0,
            'www' => 0,
          ),
        ),
        'form' => array(
          'fields' => array(
            'additional' => array(
              'collect' => 0,
              'default' => '',
              'weight' => 6,
            ),
            'city' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 8,
            ),
            'country' => array(
              'collect' => 1,
              'default' => 'us',
              'weight' => 14,
            ),
            'email' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 25,
            ),
            'fax' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 30,
            ),
            'locpick' => array(
              'collect' => 1,
              'weight' => 20,
            ),
            'name' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 2,
            ),
            'phone' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 25,
            ),
            'postal_code' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 12,
            ),
            'province' => array(
              'collect' => 0,
              'default' => '',
              'weight' => 10,
              'widget' => 'autocomplete',
            ),
            'street' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 4,
            ),
            'www' => array(
              'collect' => 1,
              'default' => '',
              'weight' => 30,
            ),
          ),
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'location',
  );

  // Exported field_base: 'field_directory_member_type'.
  $field_bases['field_directory_member_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_directory_member_type',
    'global_block_settings' => 1,
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'directory_member_types',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
