<?php
/**
 * @file
 * sector_members_directory.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sector_members_directory_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create directory_member content'.
  $permissions['create directory_member content'] = array(
    'name' => 'create directory_member content',
    'roles' => array(
      'Content Administrator' => 'Content Administrator',
      'Content Editor' => 'Content Editor',
      'Directory Member' => 'Directory Member',
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any directory_member content'.
  $permissions['delete any directory_member content'] = array(
    'name' => 'delete any directory_member content',
    'roles' => array(
      'Content Administrator' => 'Content Administrator',
      'Content Editor' => 'Content Editor',
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own directory_member content'.
  $permissions['delete own directory_member content'] = array(
    'name' => 'delete own directory_member content',
    'roles' => array(
      'Content Administrator' => 'Content Administrator',
      'Content Editor' => 'Content Editor',
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in directory_member_types'.
  $permissions['delete terms in directory_member_types'] = array(
    'name' => 'delete terms in directory_member_types',
    'roles' => array(
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any directory_member content'.
  $permissions['edit any directory_member content'] = array(
    'name' => 'edit any directory_member content',
    'roles' => array(
      'Content Administrator' => 'Content Administrator',
      'Content Editor' => 'Content Editor',
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own directory_member content'.
  $permissions['edit own directory_member content'] = array(
    'name' => 'edit own directory_member content',
    'roles' => array(
      'Content Administrator' => 'Content Administrator',
      'Content Editor' => 'Content Editor',
      'Directory Member' => 'Directory Member',
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in directory_member_types'.
  $permissions['edit terms in directory_member_types'] = array(
    'name' => 'edit terms in directory_member_types',
    'roles' => array(
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
