<?php
/**
 * @file
 * sector_members_directory.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function sector_members_directory_user_default_roles() {
  $roles = array();

  // Exported role: Directory Member.
  $roles['Directory Member'] = array(
    'name' => 'Directory Member',
    'weight' => 4,
  );

  return $roles;
}
