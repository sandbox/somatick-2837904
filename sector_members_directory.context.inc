<?php
/**
 * @file
 * sector_members_directory.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sector_members_directory_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'directory_blocks';
  $context->description = '';
  $context->tag = 'Members Directory';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'members-directory' => 'members-directory',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'facetapi-MiTxGBk1Nn5HLQT1zHoGqLOiWRKdA14f' => array(
          'module' => 'facetapi',
          'delta' => 'MiTxGBk1Nn5HLQT1zHoGqLOiWRKdA14f',
          'region' => 'pre_content',
          'weight' => '-10',
        ),
        'facetapi-BRD81QiwkLYy1wPIqUOCapOOW6m64B0W' => array(
          'module' => 'facetapi',
          'delta' => 'BRD81QiwkLYy1wPIqUOCapOOW6m64B0W',
          'region' => 'sidebar',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Members Directory');
  $export['directory_blocks'] = $context;

  return $export;
}
