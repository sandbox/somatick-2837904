defaults[projects][subdir] = "contrib"

projects[sector_members_directory][type] = module
projects[sector_members_directory][download][type] = git
projects[sector_members_directory][download][url] = https://git.drupal.org/sandbox/somatick/2837904.git 
projects[sector_members_directory][download][branch] = 7.x-1.x

projects[inline_entity_form][version] = 1.8
projects[search_api_glossary][version] = 2.2
projects[features_user_role_plus][version] = 1.2

libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woocommerce/FlexSlider/archive/2.6.3.zip"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"

projects[flexslider][version] = "2.0-rc2"
