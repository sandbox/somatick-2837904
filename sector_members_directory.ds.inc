<?php
/**
 * @file
 * sector_members_directory.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function sector_members_directory_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|directory_member|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'directory_member';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'field_directory_contacts' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Contacts',
        ),
      ),
    ),
  );
  $export['node|directory_member|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function sector_members_directory_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|directory_member|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'directory_member';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_directory_member_type',
        1 => 'body',
        2 => 'field_directory_contacts',
      ),
      'right' => array(
        3 => 'field_directory_locations',
        4 => 'field_media_collection',
      ),
    ),
    'fields' => array(
      'field_directory_member_type' => 'left',
      'body' => 'left',
      'field_directory_contacts' => 'left',
      'field_directory_locations' => 'right',
      'field_media_collection' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|directory_member|default'] = $ds_layout;

  return $export;
}
