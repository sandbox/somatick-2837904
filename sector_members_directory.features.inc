<?php
/**
 * @file
 * sector_members_directory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sector_members_directory_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function sector_members_directory_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function sector_members_directory_node_info() {
  $items = array(
    'directory_member' => array(
      'name' => t('Directory Member'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function sector_members_directory_default_search_api_index() {
  $items = array();
  $items['directory_members'] = entity_import('search_api_index', '{
    "name" : "Directory Members",
    "machine_name" : "directory_members",
    "description" : null,
    "server" : "sesame",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "directory_member" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_directory_contacts" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_directory_contacts:nid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_directory_contacts:title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_directory_contacts:vid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_directory_member_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_directory_member_type:description" : { "type" : "list\\u003Ctext\\u003E" },
        "field_directory_member_type:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_directory_member_type:parent" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_directory_member_type:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_directory_member_type:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_directory_member_type:weight" : { "type" : "list\\u003Cinteger\\u003E" },
        "nid" : { "type" : "integer" },
        "promote" : { "type" : "boolean" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "sticky" : { "type" : "boolean" },
        "title" : { "type" : "string" },
        "title_az_glossary" : { "type" : "string" },
        "vid" : { "type" : "integer" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "field_directory_member_type:parent" : "field_directory_member_type:parent",
              "field_directory_member_type:parents_all" : "field_directory_member_type:parents_all"
            }
          }
        },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_metatag_alter_callback" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_directory_contacts:title" : true,
              "field_directory_contacts:field_department" : true,
              "field_directory_contacts:field_email" : true,
              "field_directory_contacts:field_first_name" : true,
              "field_directory_contacts:field_last_name" : true,
              "field_directory_contacts:field_phone" : true,
              "field_directory_contacts:field_position" : true,
              "field_directory_contacts:field_preferred_name" : true,
              "field_directory_contacts:field_title_postfix" : true,
              "field_directory_contacts:field_title_prefix" : true,
              "field_directory_member_type:name" : true,
              "field_directory_member_type:description" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_directory_contacts:title" : true,
              "field_directory_contacts:field_department" : true,
              "field_directory_contacts:field_email" : true,
              "field_directory_contacts:field_first_name" : true,
              "field_directory_contacts:field_last_name" : true,
              "field_directory_contacts:field_phone" : true,
              "field_directory_contacts:field_position" : true,
              "field_directory_contacts:field_preferred_name" : true,
              "field_directory_contacts:field_title_postfix" : true,
              "field_directory_contacts:field_title_prefix" : true,
              "field_directory_member_type:name" : true,
              "field_directory_member_type:description" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_directory_contacts:title" : true,
              "field_directory_contacts:field_department" : true,
              "field_directory_contacts:field_email" : true,
              "field_directory_contacts:field_first_name" : true,
              "field_directory_contacts:field_last_name" : true,
              "field_directory_contacts:field_phone" : true,
              "field_directory_contacts:field_position" : true,
              "field_directory_contacts:field_preferred_name" : true,
              "field_directory_contacts:field_title_postfix" : true,
              "field_directory_contacts:field_title_prefix" : true,
              "field_directory_member_type:name" : true,
              "field_directory_member_type:description" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_directory_contacts:title" : true,
              "field_directory_contacts:field_department" : true,
              "field_directory_contacts:field_email" : true,
              "field_directory_contacts:field_first_name" : true,
              "field_directory_contacts:field_last_name" : true,
              "field_directory_contacts:field_phone" : true,
              "field_directory_contacts:field_position" : true,
              "field_directory_contacts:field_preferred_name" : true,
              "field_directory_contacts:field_title_postfix" : true,
              "field_directory_contacts:field_title_prefix" : true,
              "field_directory_member_type:name" : true,
              "field_directory_member_type:description" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 1,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_user_default_roles_plus().
 */
function sector_members_directory_user_default_roles_plus() {
  $roles = array();

  // Exported role: Directory Member
  $roles['Directory Member'] = array(
    'name' => 'Directory Member',
    'weight' => 4,
    'permissions' => array(
      'access administration menu' => TRUE,
      'access administration pages' => TRUE,
      'access contextual links' => TRUE,
      'access media browser' => TRUE,
      'access user profiles' => TRUE,
      'add media from remote sources' => TRUE,
      'create contact content' => TRUE,
      'create directory_member content' => TRUE,
      'create files' => TRUE,
      'create url aliases' => TRUE,
      'edit meta tags' => TRUE,
      'edit own audio files' => TRUE,
      'edit own contact content' => TRUE,
      'edit own directory_member content' => TRUE,
      'edit own document files' => TRUE,
      'edit own image files' => TRUE,
      'edit own video files' => TRUE,
      'moderate content from draft to needs_review' => TRUE,
      'show format selection for node' => TRUE,
      'show more format tips link' => TRUE,
      'use media wysiwyg' => TRUE,
      'use text format filtered_html' => TRUE,
      'use text format filtered_html_basic' => TRUE,
      'use workbench_moderation my drafts tab' => TRUE,
      'view own private files' => TRUE,
      'view own unpublished content' => TRUE,
      'view revisions' => TRUE,
      'view the administration theme' => TRUE,
    ),
  );

  return $roles;
}
